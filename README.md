# Тестовое задание
### Ответы на вопросы
##### 1. Для чего нужны const, let и var. В чем различия

* Ответ: данные операторы используются для объявления переменных в JavaScript. 
  * **const** - константа. Имеет блочную область видимости.
  * **var** - оператор, при использовании которого, переменная имеет глобальную область видимости, за исключением объявления внутри функции.
  * **let** - оператор, при использовании которого, переменная имеет блочную область видимости.

##### 2. Как вы понимаете термин "всплытие", почему именно такое название и что конкретно "всплывает"?
* Ответ: "всплытие" - это процесс обработки событий на элементе, при котором обрабочик сначала срабатывает на самом элементе, затем на родителе и так далее.

##### 3. Объясните термин "замыкание". К каким ошибкам приводит этот эффект? Что позволяет реализовать? Приведите пример, где использование замыкания оправдано.

* Ответ: Замыкание - это функция с возможностью получения доступа к внешним переменным и окружению.


```
function sayHelloTo(name) {
    const message = 'Hello' + name //вышестоящая функция
    return function () {
        console.log(message)
    }
}

//message для функции helloToShamil доступна из вышестоящей функции sayHelloTo
const helloToShamil = sayHelloTo('Shamil'); 

console.log(helloToShamil())
```
##### 4. Написать собственную реализацию функций reduce и bind.

* ##### reduce:
 ```
const nums = [1,2,3,4];
const reducer = (acc, cur) => acc + cur;

const result = myReduce(reducer, nums, 0);

function myReduce(reducer, arr, indexValue) {
let acc = indexValue;
for(let i = 0; i < arr.length; i++){
	const cur = arr[i];
	acc = reducer(acc,cur);
}
return acc;
}

console.log(result);
```
* ##### bind:

```
//1 Simple bind
function bind(fn, context, ...rest){
    return fn.bind(context, ...rest);
}

//2 Без встроенных методов 
function bind (fn, context, ...rest){
    return function(...args){
       const uniqId = Date.now().toString(); // уникальный id
       context[uniqId] = fn;
       const result = context[uniqId](...rest.concat(args))
       delete context[uniqId]
       return result
    }
}

//3 ES5
function bind(fn, context){
    const rest = Array.prototype.slice.call(arguments,2)
    return function(){
        const args = Array.prototype.slice.call(arguments)
        return fn.apply(context, rest.concat(args))
    }
}

//4 ES6
function bind(fn, context, ...rest){
    return function(...args){
        return fn.call(context, ...rest.concat(args))
    }
}
```
##### 5. Особенности прототипного наследования и его отличия от классического наследования в ООП.
* Ответ: При следовании парадигме прототипного наследование не обязательно создавать иерархию от общего к частному, что нельзя сказать о классической парадигме наследования в ООП.
##### 6. Для чего нужны promise и как они связаны с async/await?
* Ответ: **promise** - обертка над асинхронностью, для удобства написания кода в JavaScript. Оператор **await** выполняет функцию ожидания результата выполнения promise(упрощает работу с promise). **async** префикс у родительской функции, означает что в функции будет использоваться асинхронный код.

```
const delay = ms => {
    return new Promise(r => setTimeout(() => r(), ms))
}

const url = 'https://jsonplaceholder.typicode.com/todos/1'


//Без применения async/await
function fetchTodos() {
    console.log('Fetch todo started...')
    return delay(2000)
        .then(() => {
            return fetch(url)
        })
        .then(response => response.json())
}
fetchTodos()
    .then(data => {
        console.log('Data: ', data)
    })
    .catch(e => console.error(e))



//Код с применением async/await
async function fetchAsyncTodos() {
    console.log('Fetch todo started...')
    try {
        await delay(2000)
        const response = await fetch(url)
        const data = await response.json()
        console.log('Data', data)
    }catch (e) {
        console.error(e)
    } finally {
        console.log('finnaly')
    }
}

fetchAsyncTodos()
```

#### Сделать два калькулятора:
##### 1. Калькулятор без использования react. Нельзя применять библиотеки/фреймворки типа react, angular, vue и подобных;
##### 2. Калькулятор с использованием react.