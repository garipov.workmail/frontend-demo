let num1, num2;

function getValue(){
    num1 = parseInt(document.getElementById('n1').value);
    num2 = parseInt(document.getElementById('n2').value);
 }

 function out(result){
    document.getElementById('out').innerHTML = result;
 }

function plus(){
    getValue();
    out(num1 + num2);
}

function minus(){
    getValue();
    out(num1 - num2);
}

function multiply(){
    getValue();
    out(num1 * num2);
}

function divide(){
    getValue();
    if (num2 == 0){
     out("На ноль делить нельзя!")
    } else {
    out(num1 / num2);
    }
}


